﻿using System.Security.Cryptography;
using System.Text;
using System.Windows;
using System.Windows.Input;
using Tasker.Data.Interfaces;

namespace Tasker
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public enum WindowType
        {
            Login,
            Register
        }
        private WindowType m_type;

        //public User User { get; private set; }
        public IUser User { get; private set; }


        public LoginWindow(WindowType type)
        {
            InitializeComponent();
            switch (type)
            {
                case WindowType.Login:
                    btnAdd.Visibility = Visibility.Hidden;
                    break;
                case WindowType.Register:
                    btnLogin.Visibility = Visibility.Hidden;
                    btnNew.Visibility = Visibility.Hidden;
                    this.Title = "Register";
                    break;
            }
            // Bind the new data source to the myText TextBlock control's Text dependency property.
            m_type = type;
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var sha512 = new SHA512CryptoServiceProvider();
                var password = sha512.ComputeHash(Encoding.UTF8.GetBytes(pwbPwd.Password));

                User = App.Factory.Login(txtLogin.Text, password);
            }
            finally
            {
                this.Close();
            }
        }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            LoginWindow wnd = new LoginWindow(WindowType.Register);
            wnd.ShowDialog();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            var sha512 = new SHA512CryptoServiceProvider();


            IUser local_user = App.Factory.CreateUser();
            local_user.UserName = txtLogin.Text;
            local_user.Password = sha512.ComputeHash(Encoding.UTF8.GetBytes(pwbPwd.Password));

            local_user.Save();
            this.Close();
        }

        private void pwbPwd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && btnLogin.Visibility == Visibility.Visible)
            {
                btnLogin_Click(sender, new RoutedEventArgs());
                e.Handled = true;
            }

        }
    }
}
