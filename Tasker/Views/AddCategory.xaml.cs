﻿using System.Windows.Controls;
using Tasker.ViewModels;

namespace Tasker.Views
{
    /// <summary>
    /// Interaction logic for AddCategory.xaml
    /// </summary>
    public partial class AddCategory : UserControl
    {
        public AddCategory()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Visibility = System.Windows.Visibility.Hidden;
            ((CategoryViewModel)DataContext).Save();
        }
    }
}
