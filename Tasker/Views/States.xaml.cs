﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tasker.ViewModels;

namespace Tasker.Views
{
    /// <summary>
    /// Interaction logic for States.xaml
    /// </summary>
    public partial class States : UserControl
    {
        public States()
        {
            InitializeComponent();
        }

        private void ButtonEditSettings_DefaultButtonClick(object sender, RoutedEventArgs e)
        {
           if(sender is StateViewModel svm)
            {
                svm.Delete();
            }
        }
    }
}
