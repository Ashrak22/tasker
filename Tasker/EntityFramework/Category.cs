﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Tasker.EFData
{
    [Obsolete("Use XPO classes in Tasker.XPOData")]
	public class Category
	{
		public int CategoryID { get; set; }
		public User User { get; set; }
		public string Name { get; set; }

		public virtual ICollection<Task> Tasks { get; set; }

		public Category()
		{
			Tasks = new ObservableCollection<Task>();
		}
	}
}
