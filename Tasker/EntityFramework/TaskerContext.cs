﻿using System;
using System.Data.Entity;

namespace Tasker.EFData
{
    [Obsolete("Use XPO classes in Tasker.XPOData")]
    class TaskerContext : DbContext
	{
		public TaskerContext(): base()
		{
			//For development purposes
			Database.SetInitializer<TaskerContext>(new DropCreateDatabaseIfModelChanges<TaskerContext>());
			Configuration.LazyLoadingEnabled = true;
			Configuration.ProxyCreationEnabled = true;
		}
		public DbSet<User> Users { get; set; }
	}
}
