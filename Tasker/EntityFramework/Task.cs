﻿using System;

namespace Tasker.EFData
{
    [Obsolete("Use XPO classes in Tasker.XPOData")]
    public class Task
	{
		public int TaskID { get; set; }
		public User User { get; set; }
		public string Text { get; set; }
		public DateTime Created { get; set; }
		public DateTime Expires { get; set; }
		public Category Category { get; set; }

		public Task()
		{

		}
	}
}
