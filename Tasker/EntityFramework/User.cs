﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Tasker.EFData
{
    [Obsolete("Use XPO classes in Tasker.XPOData")]
    public class User
	{
		public int UserID { get; set; }
		public string UserName { get; set; }
		public byte[] Password { get; set; }

		public virtual ICollection<Category> Categories { get; set; }
		public virtual ICollection<Task> Tasks { get; set; }

		public User() {
			Categories = new ObservableCollection<Category>();
			Tasks = new ObservableCollection<Task>();
		}

		
	}

}
