﻿using DevExpress.Mvvm;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using Tasker.Data.Interfaces;

namespace Tasker.ViewModels
{
    public class CategoryViewModel : ViewModelBase
    {
        private ICategory _category;
        private ObservableCollection<StateViewModel> _states;

        private ICommand _deleteCommand;
        private ICommand _saveCommand;
        private ICommand _newTaskCommand;


        public CategoryViewModel(ICategory category, ObservableCollection<StateViewModel> states)
        {
            _category = category;
            _states = states;
            Tasks = createList();
        }

        public string Name
        {
            get { return _category.Name; }
            set
            {
                if (value != _category.Name)
                    _category.Name = value;
            }
        }

        public ICommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new DelegateCommand(Delete);
                }
                return _deleteCommand;

            }
        }
        public ICommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new DelegateCommand(Save);
                }
                return _saveCommand;
            }
        }
        public ICommand NewTaskCommand
        {
            get
            {
                if (_newTaskCommand == null)
                {
                    _newTaskCommand = new DelegateCommand(NewTask);
                }
                return _newTaskCommand;
            }
        }
        
        public ObservableCollection<TaskViewModel> Tasks { get; } = null;

        public void Save()
        {
            _category.Save();
            RaisePropertyChanged("Save");
        }
        public void Delete()
        {
            _category.Delete();
            RaisePropertyChanged("Delete");
        }

        #region Helper Functions
        private void NewTask() => Tasks.Add(prepareTask(new TaskViewModel(_category, _states)));

        private ObservableCollection<TaskViewModel> createList()
        {
            var res = new ObservableCollection<TaskViewModel>();
            foreach (var tas in _category?.Tasks)
                res.Add(prepareTask(new TaskViewModel(tas, _states)));

            return res;
        }

        private TaskViewModel prepareTask(TaskViewModel task)
        {
            task.PropertyChanged += Task_PropertyChanged;
            return task;
        }

        private void Task_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "Delete":
                    Tasks.Remove(sender as TaskViewModel);
                    break;
                case "IsEditable":
                case "IsExpired":
                    RaisePropertyChanged("Refresh");
                    break;
            }
            RaisePropertyChanged("Tasks");
        }
        #endregion
    }
}