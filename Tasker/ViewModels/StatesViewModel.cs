﻿using DevExpress.Mvvm;
using System;
using System.Windows.Input;
using System.Windows.Media;
using Tasker.Data.Interfaces;

namespace Tasker.ViewModels
{
    public class StateViewModel : ViewModelBase
    {
        private IState _state;
        private ICommand _deleteCommand;

        public StateViewModel(IState state)
        {
            _state = state;
        }

        public string Name { get { return _state.Name; }
            set {
                if(value != _state.Name)
                {
                    _state.Name = value;
                    RaisePropertyChanged("Name");
                }
            }
        }
        public short Time_Minutes { get { return _state.Time_Minutes; }
            set
            {
                if(value != _state.Time_Minutes)
                {
                    _state.Time_Minutes = value;
                    RaisePropertyChanged("Time_Minutes");
                }
            }
        }
        public ICommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new DelegateCommand(Delete);
                }
                return _deleteCommand;

            }
        }

        internal void Delete()
        {
            _state.Delete();
            RaisePropertyChanged("Deleted");
        }

        public short Sort
        {
            get { return _state.Sort; }
            set
            {
                if (value != _state.Sort)
                {
                    _state.Sort = value;
                    RaisePropertyChanged("Sort");
                }
            }
        }

        public Color Color {
            get {
                if(_state.Color == null)
                    return Colors.White;

                var nums = _state.Color.Split('-');
                return Color.FromRgb(byte.Parse(nums[0]), byte.Parse(nums[1]), byte.Parse(nums[2]));
            }
            set
            {
                string new_value = $"{value.R}-{value.G}-{value.B}";
                if(new_value != _state.Color)
                {
                    _state.Color = new_value;
                    RaisePropertyChanged("Color");
                }
            }
        }

        internal void Save()
        {
            _state.Save();
        }
    }
}