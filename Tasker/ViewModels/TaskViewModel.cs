﻿using DevExpress.Mvvm;
using System;
using System.ComponentModel;
using System.Windows.Media;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Tasker.Data.Interfaces;
using System.Collections.ObjectModel;

namespace Tasker.ViewModels
{
    /// <summary>
    /// ViewModel for Tasks. It fires Property changed for IsExpired each Minute, so that we can AutoUpdate Tasks as Expired
    /// </summary>
    public class TaskViewModel : ViewModelBase
    {
        private ITask _task;
        private ICommand _deleteCommand;
        private ICommand _saveCommand;
        private ICommand _editCommand;
        private bool _isEditable;
        private bool _isNew;
        private Timer _t;
        private Color _color;
        private ObservableCollection<StateViewModel> _states;

        public TaskViewModel(ITask task, ObservableCollection<StateViewModel> states)
        {
            _task = task;
            _states = states;
            IsEditable = false;
            _isNew = false;
            if(!IsExpired)
                createExpiryAnnouncer();
            SetState();
        }

        //New Task
        public TaskViewModel(ICategory category, ObservableCollection<StateViewModel> states)
        {
            _task = App.Factory.CreateTask();
            _task.SetCategory(category);
            _task.Created = DateTime.UtcNow;
            _task.Expires = DateTime.UtcNow.AddMinutes(2);
            _states = states;

            IsEditable = true;
            _isNew = true;
            SetState();
        }

        public string Title
        {
            get { return _task.Title; }
            set
            {
                if (_task.Title != value)
                {
                    _task.Title = value;
                    RaisePropertyChanged("Title");
                }
            }
        }
        public string Text
        {
            get { return _task.Text; }
            set
            {
                if (value != _task.Text)
                {
                    _task.Text = value;
                    RaisePropertyChanged("Name");
                }
            }
        }
        public DateTime Created => _task.Created;
        public DateTime Expires
        {
            get { return _task.Expires; }
            set
            {
                if (value != _task.Expires)
                {
                    _task.Expires = value;
                    RaisePropertyChanged("Expires" +
                        "");
                }
            }
        }

        public Brush Color { get {
                _color.A = 30;
                return new SolidColorBrush(_color);
            }
        }

        public ICommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new DelegateCommand(Delete);
                }
                return _deleteCommand;

            }
        }
        public ICommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new DelegateCommand(Save);
                }
                return _saveCommand;
            }
        }
        public ICommand EditCommand
        {
            get
            {
                if (_editCommand == null)
                {
                    _editCommand = new DelegateCommand(Edit);
                }
                return _editCommand;

            }
        }

        public bool IsExpired { get { return _task.Expires < DateTime.UtcNow; } }
        public bool IsEditable
        {
            get { return _isEditable; }
            set
            {
                if(_isEditable != value)
                {
                    _isEditable = value;
                    OnRaisePropertyChanged("IsEditable");
                    SetState();
                }
            }
        }
        public string State { get; private set; }
        public int Sort { get; private set; }

        private void Save()
        {
            _task.Save();
            IsEditable = false;
            _isNew = false;
            RaisePropertyChanged("Save");
            createExpiryAnnouncer();
        }
        private void Delete()
        {
            if (!_isNew)
                _task.Delete();
            RaisePropertyChanged("Delete");
        }
        private void Edit()
        {
            IsEditable = true;
        }

        /// <summary>
        /// Counts the number of milliseconds until the next full minute
        /// </summary>
        /// <returns>Number of milliseconds</returns>
        private static double calculateInterval()
        {
            DateTime now = DateTime.UtcNow;
            return (((now.Second > 30 ? 120:60) -now.Second) * 1000 - now.Millisecond);
        }

        /// <summary>
        /// Adds an expiry Timer to task, that checks every Minute whether the task has already expired
        /// </summary>
        private void createExpiryAnnouncer()
        {
            _t = new Timer
            {
                AutoReset = false, //To keep it from drifting we'll calculate the interval each time
                Interval = calculateInterval()
            };

            _t.Elapsed += (o, e) =>
            {
                if (!IsExpired)
                {
                    _t.Interval = calculateInterval();
                    _t.Start();
                }
                else
                {
                    _t = null;
                    OnRaisePropertyChanged("IsExpired"); //Once task expires we don't need the timer anymore
                }
            };
            _t.Start();
        }

        
        
        private void SetState()
        {
            if (IsEditable)
            {
                State = "New/Edit";
                Sort = 0;
            }
            else if (IsExpired)
            {
                State = "Expired";
                Sort = 100;
                _color = Colors.White;
            }
            else
            {
                State = "New";
                Sort = 0;
                _color = Colors.Aqua;
                foreach (var state in _states)
                {
                    TimeSpan expiry = Expires - DateTime.UtcNow;
                    if (expiry.TotalMinutes > state.Time_Minutes)
                        break;

                    State = state.Name;
                    Sort = state.Sort;
                    _color = state.Color;
                }
            }
        }

        private void OnRaisePropertyChanged(string property)
        {
            //We don't need to check the property name, since we call this only for IsEdited and IsExpired
           SetState();
           RaisePropertiesChanged(property, "State", "Sort", "Color");
        }

    }

    public sealed class TaskTemplateSelector : DataTemplateSelector
    {
        public DataTemplate TaskTemplate { get; set; }
        public DataTemplate EditableTaskTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if(item is TaskViewModel m)
            {
                void lambda(object o, PropertyChangedEventArgs e)
                {
                    if (e.PropertyName == "IsEditable")
                    {
                        m.PropertyChanged -= lambda;
                        var cp = (ContentPresenter)container;
                        cp.ContentTemplateSelector = null;
                        cp.ContentTemplateSelector = this;
                    }
                }

                m.PropertyChanged += lambda;
            }
            if (!(item is TaskViewModel task))
                return base.SelectTemplate(item, container);
            else if (task.IsEditable)
                return EditableTaskTemplate;
            else
                return TaskTemplate;
        }
    }
}
