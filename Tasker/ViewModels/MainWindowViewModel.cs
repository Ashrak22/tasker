﻿using DevExpress.Mvvm;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using Tasker.Data.Interfaces;

namespace Tasker.ViewModels
{
    class MainWindowViewModel : ViewModelBase
    {
        private IUser _user;

        private ICommand _newStateCommand;
        private ICommand _saveStatesCommand;

        public string UserName => _user?.UserName ?? "";
        public CategoryViewModel CurrentCategory { get; set; } = null;
        public CategoryViewModel NewCategory {
            get {
                var category = App.Factory.CreateCategory();
                category.SetUser(_user);
                return prepareCategoryViewModel(category);
            }
        }
        public ObservableCollection<CategoryViewModel> Categories { get; set; } = null;
        public ObservableCollection<StateViewModel> States { get; set; } = null;

        public ICommand NewStateCommand
        {
            get
            {
                if (_newStateCommand == null)
                {
                    _newStateCommand = new DelegateCommand(NewState);
                }
                return _newStateCommand;

            }
        }

        public ICommand SaveStatesCommand
        {
            get
            {
                if (_saveStatesCommand == null)
                {
                    _saveStatesCommand = new DelegateCommand(SaveStates);
                }
                return _saveStatesCommand;

            }
        }

        public MainWindowViewModel()
        {
            
        }

        public bool Login()
        {
            //TODO: Move Login Logic to ViewModel
            LoginWindow wnd = new LoginWindow(LoginWindow.WindowType.Login);
            wnd.ShowDialog();
            _user = wnd.User;

            States = createStateList();
            Categories = createList();
            RaisePropertiesChanged("UserName", "Categories");

            return !(_user == null);
        }

        public bool Logout()
        {
            return false;
        }

        private void NewState()
        {
            var state = App.Factory.CreateState();
            state.SetUser(_user);
            States.Add(prepareStateViewModel(state));
        }

        private void SaveStates()
        {
            foreach (var state in States)
                state.Save();
        }
    
        #region Helper Functions
        private ObservableCollection<CategoryViewModel> createList()
        {
            var res = new ObservableCollection<CategoryViewModel>();
            if (_user == null)
                return null;
            foreach (var category in _user?.Categories)
            {
                res.Add(prepareCategoryViewModel(category));
            }
                
            return res;
        }

        private CategoryViewModel prepareCategoryViewModel(ICategory category)
        {
            var cat = new CategoryViewModel(category, States);
            cat.PropertyChanged += Category_PropertyChanged;
            return cat;
        }

        private void Category_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "Save":
                    Categories.Add(sender as CategoryViewModel);
                    break;
                case "Delete":
                    Categories.Remove(sender as CategoryViewModel);
                    break;
            }
            RaisePropertyChanged("Categories");
        }

        private ObservableCollection<StateViewModel> createStateList()
        {
            var res = new ObservableCollection<StateViewModel>();
            if (_user == null)
                return null;
            foreach (var state in _user?.States)
            {
                res.Add(prepareStateViewModel(state));
            }

            return res;
        }

        private StateViewModel prepareStateViewModel(IState state)
        {
            var sta = new StateViewModel(state);
            sta.PropertyChanged += Category_PropertyChanged;
            return sta;
        }

        private void State_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "Deleted":
                    States.Remove(sender as StateViewModel);
                    break;
            }
            RaisePropertyChanged("States");
        }
        #endregion
    }
}
