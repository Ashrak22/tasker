﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Tasker.EFData;

namespace Tasker.ViewModels
{
    [Obsolete("Use MainWindowViewModel")]
    class MainWindowViewModelEF : INotifyPropertyChanged, IDisposable
    {
        private User _user = null;
        private TaskerContext ctx = new TaskerContext();

        public User User
        {
            get { return _user; }
            set
            {
                if (!(_user?.Equals(value) ?? false))
                {
                    _user = value;
                    if (_user != null)
                        ctx.Users.Attach(_user);
                    OnPropertyChanged("UserName");
                    OnPropertyChanged("Categories");
                }
            }
        }
        public string UserName => _user?.UserName ?? "";

        //Category Support
        public ICollection<Category> Categories => _user?.Categories;
        public Category NewCategory => new Category();
        public Category CurrentCategory => Categories.ElementAt(SelectedIndex);
        public int SelectedIndex { get; set; } = -1;

        //Task Support
        public Task NewTask => new Task();

        public void SaveCategory(Category to_save)
        {
            Categories.Add(to_save);
            ctx.SaveChanges();
        }

        public void DeleteCategory(Category to_delete)
        {
            Categories.Remove(to_delete);
            ctx.SaveChanges();
        }

        public void SaveTask(Task task)
        {
            if(SelectedIndex != -1)
            {
                task.Created = DateTime.UtcNow;
                CurrentCategory.Tasks.Add(task);
                ctx.SaveChanges();
            }
        }

        #region INotifyPropertyChanged Support
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion
        #region IDisposable Support
        private bool disposedValue = false; // Zjištění redundantních volání

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        #endregion
    }
}
