﻿using DevExpress.Xpo;
using System;
using Tasker.Data.Interfaces;

namespace Tasker.XPOData.Tasker_Data_TaskerContext
{

    public partial class States : IState
    {
        public States(Session session) : base(session) { }
        public override void AfterConstruction() { base.AfterConstruction(); }

        public void SetUser(IUser user)
        {
            if (!(user is Users usr))
                throw new TypeAccessException("Wrong type");
            else
                User = usr;
        }
    }

}
