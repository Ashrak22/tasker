﻿using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using Tasker.Data.Interfaces;

namespace Tasker.XPOData.Tasker_Data_TaskerContext
{

    public partial class Categories : ICategory
    {
        public Categories(Session session) : base(session) { }
        public override void AfterConstruction() { base.AfterConstruction(); }


        [NonPersistent]
        public IEnumerable<ITask> Tasks => TasksCollection;

        public void SetUser(IUser user)
        {
            if (!(user is Users usr))
                throw new TypeAccessException("Wrong type");
            else
                User_UserID = usr;
        }

    }

}
