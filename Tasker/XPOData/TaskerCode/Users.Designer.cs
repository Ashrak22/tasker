﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using DevExpress.Xpo;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
namespace Tasker.XPOData.Tasker_Data_TaskerContext
{

    public partial class Users : XPLiteObject
    {
        int fUserID;
        [Key(true)]
        public int UserID
        {
            get { return fUserID; }
            set { SetPropertyValue<int>(nameof(UserID), ref fUserID, value); }
        }
        string fUserName;
        [Size(SizeAttribute.Unlimited)]
        public string UserName
        {
            get { return fUserName; }
            set { SetPropertyValue<string>(nameof(UserName), ref fUserName, value); }
        }
        byte[] fPassword;
        [Size(SizeAttribute.Unlimited)]
        [MemberDesignTimeVisibility(true)]
        public byte[] Password
        {
            get { return fPassword; }
            set { SetPropertyValue<byte[]>(nameof(Password), ref fPassword, value); }
        }
        [Association(@"CategoriesReferencesUsers")]
        public XPCollection<Categories> CategoriesCollection { get { return GetCollection<Categories>(nameof(CategoriesCollection)); } }
        [Association(@"StatesReferencesUsers"), Aggregated]
        public XPCollection<States> StatesCollection { get { return GetCollection<States>(nameof(StatesCollection)); } }
    }

}
