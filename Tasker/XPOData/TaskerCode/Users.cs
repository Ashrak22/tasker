﻿using DevExpress.Xpo;
using System.Collections.Generic;
using Tasker.Data.Interfaces;

namespace Tasker.XPOData.Tasker_Data_TaskerContext
{
    public partial class Users : IUser
    {
        public Users(Session session) : base(session) { }
        public override void AfterConstruction() { base.AfterConstruction(); }

        [NonPersistent]
        public IEnumerable<ICategory> Categories => CategoriesCollection;

        [NonPersistent]
        public IEnumerable<IState> States
        {
            get
            {
                StatesCollection.Sorting = new SortingCollection(new SortProperty("Time_Minutes", DevExpress.Xpo.DB.SortingDirection.Descending));
                return StatesCollection;
            }
        }
    }
}
