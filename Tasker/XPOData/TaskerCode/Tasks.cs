﻿using DevExpress.Xpo;
using System;
using Tasker.Data.Interfaces;

namespace Tasker.XPOData.Tasker_Data_TaskerContext
{

    public partial class Tasks : ITask
    {
        public Tasks(Session session) : base(session) { }
        public override void AfterConstruction() { base.AfterConstruction(); }

        public void SetCategory(ICategory category)
        {
            if (!(category is Categories ctgr))
                throw new TypeAccessException("Wrong type");
            else
                Category_CategoryID = ctgr;
        }
    }

}
