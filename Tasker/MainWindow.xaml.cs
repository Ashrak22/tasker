﻿using DevExpress.Xpf.Ribbon;
using System.Windows;
using Tasker.ViewModels;

namespace Tasker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : DXRibbonWindow
    {
        private MainWindowViewModel viewModel = new MainWindowViewModel();
        
        public MainWindow()
        {
            InitializeComponent();
            DataContext = viewModel;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!viewModel.Login())
                this.Close();
        }

        private void acAdd_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var value = e.NewValue as bool?;
            if (!(value ?? true))
                tcCategories.Visibility = Visibility.Visible;
        }

        private void btnNewCategory_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e) => newCategory();

        private void btnExit(object sender, RoutedEventArgs e) => this.Close();

        private void newCategory()
        {
            acAdd.Visibility = Visibility.Visible;
            tcCategories.Visibility = Visibility.Hidden;
            acAdd.DataContext = viewModel.NewCategory;
        }

        private void tcCategories_SelectionChanged(object sender, DevExpress.Xpf.Core.TabControlSelectionChangedEventArgs e)
        {
            if (e.NewSelectedIndex == -1)
                rpcTask.IsVisible = false;
            else
                rpcTask.IsVisible = true;
        }

        private void btnState_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            tcCategories.Visibility = Visibility.Hidden;
            stStates.DataContext = viewModel.States;
            stStates.Visibility = Visibility.Visible;

        }

        private void stState_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var value = e.NewValue as bool?;
            if (!(value ?? true))
            {
                rpcState.IsVisible = false;
                tcCategories.Visibility = Visibility.Visible;
                if (tcCategories.SelectedIndex != -1)
                    rpcTask.IsVisible = true;
            }
            else
            {
                rpcState.IsVisible = true;
                rpcTask.IsVisible = false;
            }
        }

        private void btnCloseStates_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            stStates.Visibility = Visibility.Hidden;
            tcCategories.Visibility = Visibility.Visible;
        }
    }
}
