﻿using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using System;
using System.Windows;
using Tasker.Data.Interfaces;
using Tasker.XPOData.Tasker_Data_TaskerContext;

namespace Tasker
{
	class XPOFactory : IFactory
	{
		private Session _session;

		public XPOFactory()
		{
			ConnectionHelper.Connect(DevExpress.Xpo.DB.AutoCreateOption.DatabaseAndSchema, true);
			_session = new Session();
		}

		public ICategory CreateCategory()
		{
			return new Categories(_session);
		}

		public ITask CreateTask()
		{
			return new Tasks(_session);
		}

		public IUser CreateUser()
		{
			return new Users(_session);
		}

		public IState CreateState()
		{
			return new States(_session);
		}

		public IUser Login(string userName, byte[] password)
		{
			var op_final = new GroupOperator(GroupOperatorType.And,
							new BinaryOperator("UserName", userName, BinaryOperatorType.Equal),
							new BinaryOperator("Password", password, BinaryOperatorType.Equal));
			return _session.FindObject<Users>(op_final) ?? throw new Exception("Invalid login");
		}

		#region IDisposable Support
		private bool disposedValue = false;

		protected void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					_session.Dispose();
				}

				disposedValue = true;
			}
		}

		public void Dispose()
		{
			Dispose(true);
		}
		#endregion
	}
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		
		public static IFactory Factory { get; private set; }    

		private void Application_Startup(object sender, StartupEventArgs e)
		{
			Factory = new XPOFactory();

			MainWindow window = new MainWindow();
			MainWindow = window;
			MainWindow.Show();
		}

		private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
		{
			MessageBox.Show("An unhandled exception just occurred: " + e.Exception.Message, "Exception Sample", MessageBoxButton.OK, MessageBoxImage.Error);
			e.Handled = true;
			App.Current.Shutdown();
		}

		private void Application_Exit(object sender, ExitEventArgs e)
		{
			Factory?.Dispose();
		}
	}


}
