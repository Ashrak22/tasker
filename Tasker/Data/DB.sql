﻿SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users] (
	[UserID]   INT             IDENTITY (1, 1) NOT NULL,
	[UserName] NVARCHAR (255)  NULL,
	[Password] VARBINARY (MAX) NULL,
	CONSTRAINT [PK_dbo.Users] PRIMARY KEY CLUSTERED ([UserID] ASC)
);

GO
CREATE UNIQUE NONCLUSTERED INDEX [iUserName_Users]
	ON [dbo].[Users]([UserName] ASC);
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories] (
	[CategoryID]  INT            IDENTITY (1, 1) NOT NULL,
	[Name]        NVARCHAR (MAX) NULL,
	[User_UserID] INT            NULL,
	CONSTRAINT [PK_dbo.Categories] PRIMARY KEY CLUSTERED ([CategoryID] ASC),
	CONSTRAINT [FK_dbo.Categories_dbo.Users_User_UserID] FOREIGN KEY ([User_UserID]) REFERENCES [dbo].[Users] ([UserID])
);


GO
CREATE NONCLUSTERED INDEX [IX_User_UserID]
	ON [dbo].[Categories]([User_UserID] ASC);


GO
CREATE TRIGGER dbo.[TRG_Category_Delete]
	ON [dbo].[Categories]
	FOR DELETE
	AS
	BEGIN
		delete from Tasks
		from Tasks t
			inner join deleted d on d.CategoryID = t.Category_CategoryID
	END

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tasks] (
	[TaskID]              INT            IDENTITY (1, 1) NOT NULL,
	[Text]                NVARCHAR (MAX) NULL,
	[Created]             DATETIME       NOT NULL,
	[Expires]             DATETIME       NOT NULL,
	[Category_CategoryID] INT            NULL,
	[Title]               NVARCHAR (300) NULL,
	CONSTRAINT [PK_dbo.Tasks] PRIMARY KEY CLUSTERED ([TaskID] ASC),
	CONSTRAINT [FK_dbo.Tasks_dbo.Categories_Category_CategoryID] FOREIGN KEY ([Category_CategoryID]) REFERENCES [dbo].[Categories] ([CategoryID]),
	CONSTRAINT [FK_dbo.Tasks_dbo.Users_User_UserID] FOREIGN KEY ([User_UserID]) REFERENCES [dbo].[Users] ([UserID])
);


GO
CREATE NONCLUSTERED INDEX [IX_Category_CategoryID]
	ON [dbo].[Tasks]([Category_CategoryID] ASC);
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[States] (
	[StateID]      NVARCHAR (100) NOT NULL,
	[Name]         NVARCHAR (100) NULL,
	[Time_Minutes] SMALLINT       NULL,
	[User]         INT            NULL
);


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[States] (
	[StateID]      INT NOT NULL IDENTITY,
	[Name]         NVARCHAR (100) NULL,
	[Time_Minutes] SMALLINT       NULL,
	[User]         INT            NULL,
	[Sort]         SMALLINT       NULL,
	[Color]        NVARCHAR (100) NULL,
	CONSTRAINT [PK_States] PRIMARY KEY CLUSTERED ([StateID] ASC),
	CONSTRAINT [FK_States_User] FOREIGN KEY ([User]) REFERENCES [dbo].[Users] ([UserID]) NOT FOR REPLICATION
);


GO
CREATE NONCLUSTERED INDEX [iUser_States]
	ON [dbo].[States]([User] ASC);
