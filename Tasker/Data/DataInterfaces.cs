﻿using System;
using System.Collections.Generic;

/// <summary>
/// Interfaces for Data Decoupling. Model classes can be anything as long as they satisfy the proper interface
/// </summary>
namespace Tasker.Data.Interfaces
{
    public interface IBaseData
    {
        void Save();
        void Delete();
    }

    public interface IUser : IBaseData
    {
        string UserName { get; set; }
        byte[] Password { get; set; }
        IEnumerable<ICategory> Categories { get; }
        IEnumerable<IState> States { get; }
    }

    public interface ICategory : IBaseData
    {
        string Name { get; set; }

        IEnumerable <ITask> Tasks { get; }

        void SetUser(IUser user);
    }

    public interface ITask : IBaseData
    {
        string Title { get; set; }
        string Text { get; set; }
        DateTime Created { get; set; }
        DateTime Expires { get; set; }

        void SetCategory(ICategory category);
    }

    public interface IState : IBaseData
    {
        string Name { get; set; }
        short Time_Minutes { get; set; }
        string Color { get; set; }
        short Sort { get; set; }

        void SetUser(IUser user);
    }

    public interface IFactory : IDisposable
    {
        IUser Login(string userName, byte[] password);
        IUser CreateUser();
        ICategory CreateCategory();
        ITask CreateTask();
        IState CreateState();
    }
}
